//
//  PersistentCacheProvider.swift
//  AppPortal
//
//  Created by Gian Nucci on 09/03/18.
//  Copyright © 2018 Rede. All rights reserved.
//

import Foundation

// MARK: Persistent cache

public final class PersistentCacheProvider: NSObject, PersistentCacheProtocol {
    
    // MARK: - Properties

    private let persistentKey = "17257c37-4c8a-4b2f-829e-76e96a56e092"
    private let valueKey = "value"

    private lazy var defaults: UserDefaults = {
        guard let defaults = UserDefaults(suiteName: persistentKey) else {
            let defaults = UserDefaults()
            defaults.addSuite(named: persistentKey)
            return defaults
        }
        return defaults
    }()

    // MARK: - Persistent Cache Protocol

    /// Count of current cached items
    public var count: Int {
        return defaults.dictionaryRepresentation().count
    }

    /// Remove all cached items
    public func clear() {
        defaults.removePersistentDomain(forName: persistentKey)
        defaults.synchronize()
    }

    /// Remove especific cached item
    ///
    /// - Parameter key: String
    public func removeValue(forKey key: String) -> Any? {
        guard let recoveredValue = defaults.value(forKey: key) as? [String: Any?] else { return nil }

        defaults.removeObject(forKey: key)
        defaults.synchronize()
        return recoveredValue[valueKey] as Any?
    }

    /// Verify if cache contains cache
    ///
    /// - Parameter key: String
    public func contains(_ key: String) -> Bool {
        let value = defaults.value(forKey: key)
        return value != nil
    }

    /// Subscription access for Getter and Setter
    ///
    /// - Parameter key: String
    public subscript(key: String) -> Any? {
        get {
            guard let item = defaults.value(forKey: key) as? [String: Any?] else { return nil }
            return item[valueKey] as Any?
        }
        set(value) {
            let item = [valueKey: value]

            defaults.set(item, forKey: key)
            defaults.synchronize()
        }
    }
}
