//
//  PersistentCacheProtocol.swift
//  AppPortal
//
//  Created by Gian Nucci on 09/03/18.
//  Copyright © 2018 Rede. All rights reserved.
//

import Foundation

public protocol PersistentCacheProtocol {
    /// Count of current cached items
    var count: Int { get }

    /// Remove all cached items
    func clear()

    /// Remove especific cached item
    ///
    /// - Parameter key: String
    func removeValue(forKey key: String) -> Any?

    /// Verify if cache contains cache
    ///
    /// - Parameter key: String
    func contains(_ key: String) -> Bool

    /// Subscription access for Getter and Setter
    ///
    /// - Parameter key: String
    subscript(key: String) -> Any? { get set }
}
