//
//  Cache.swift
//  MyLocations
//
//  Created by Renato Miranda de Assis on 01/09/19.
//  Copyright © 2019 Renato Miranda. All rights reserved.
//

import Foundation

public final class Cache {
    
    // MARK: - Properties
    
    private var persistent: PersistentCacheProtocol = PersistentCacheProvider()
    /// Shared instance
    public static let shared = Cache()
    
    // MARK: - Public Methods
    
    /// Make use of this method to inject custom Session and Persistent Cache Protocols instances
    ///
    /// - Parameters:
    ///   - persistentProvider: PersistentCacheProvider
    public static func setup(persistentProvider: PersistentCacheProtocol? = nil) {
        shared.persistent = persistentProvider ?? PersistentCacheProvider()
    }
    
    public func count() -> Int {
        return persistent.count
    }
    
    /// Add new cache value for informed type and key
    /// Objects added on cache should be representes as Plist
    /// Make use of PropertyListEncoder and PropertyListDecoder if necessary
    ///
    /// - Parameters:
    ///   - value: Any nullable
    ///   - key: String
    public func add(value: Codable?, forKey key: String) {
        persistent[key] = value
    }
    
    /// Retrieve value from key and type informed
    ///
    /// - Parameters:
    ///   - key: String
    /// - Returns: Any nullable
    public func value(forKey key: String) -> Any? {
        return persistent[key]
    }
    
    /// Remove value from key and type informed
    ///
    /// - Parameters:
    ///   - key: String
    /// - Returns: Any nullable.
    public func removeValue(forKey key: String) -> (Any?, Any?) {
        return (nil, persistent.removeValue(forKey: key))
    }
    
    /// Verify if cache has an specific key
    /// In case o type .both being informed, the return will be true in case of it be found in any of the types
    ///
    /// - Parameters:
    ///   - key: String
    /// - Returns: Bool
    public func contains(_ key: String) -> Bool {
        return persistent.contains(key)
    }
    
    /// Remove all cache from an specific type
    public func clear() {
        persistent.clear()
    }
}
