//
//  UserDefaultsCacheProtocol.swift
//  AppPortal
//
//  Created by João Pedro Pinto Coelho on 09/10/18.
//  Copyright © 2018 Rede. All rights reserved.
//

import Foundation

protocol UserDefaultsCacheProtocol {
    subscript<T>(key: String) -> T? { get set }
}
