//
//  UserDefaultsCacheProvider.swift
//  AppPortal
//
//  Created by João Pedro Pinto Coelho on 09/10/18.
//  Copyright © 2018 Rede. All rights reserved.
//

import Foundation

class UserDefaultsCacheProvider: NSObject, UserDefaultsCacheProtocol {
    subscript<T>(key: String) -> T? {
        get {
            if let defaults = UserDefaults.standard.object(forKey: key) as? T {
                return defaults
            } else {
                return nil
            }
        }
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: key)
        }
    }
}
