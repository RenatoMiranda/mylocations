//
//  MapProvider.swift
//  MyLocations
//
//  Created by Renato Miranda de Assis on 31/08/19.
//  Copyright © 2019 Renato Miranda. All rights reserved.
//

import Alamofire
import Foundation

class MapProvider: MapProviderProtocol {

    private static let favoriteListKey = "favoriteList"
    
    func suggestedItems(searchText: String, completion: @escaping SuggestionsCallback) {
        let textQuery = searchText.split(separator: " ").joined(separator: "+")
        
        let url = "\(AppDelegate.suggestionUrl)?app_id=\(AppDelegate.appId)&jsonattributes=1&app_code=\(AppDelegate.appCode)&query=\(textQuery)"
        AF.request(url).responseData { response in
            print(response)
            switch response.result {
            case .success(let data):
                let locationResponse = try? JSONDecoder().decode(SuggestionsResponse.self, from: data)
                completion(locationResponse?.suggestions, nil)
            case .failure(let error):
                completion(nil, error.asAFError)
            }
        }
    }
    
    func location(locationId: String, completion: @escaping LocationCallback) {
        let recoveredValue = Cache.shared.value(forKey: locationId) as? Data
        if let recovered = recoveredValue, let location = try? PropertyListDecoder().decode(LocationItem.self, from: recovered) {
            completion(location, nil)
        } else {
            let url = "\(AppDelegate.locationUrl)?locationid=\(locationId)&jsonattributes=1&gen=9&app_id=\(AppDelegate.appId)&app_code=\(AppDelegate.appCode)"
            AF.request(url).responseData { response in
                print(response)
                switch response.result {
                case .success(let data):
                    let locationResponse = try? JSONDecoder().decode(LocationResponse.self, from: data)
                    completion(locationResponse?.response.view.first?.result.first?.location, nil)
                case .failure(let error):
                    completion(nil, error.asAFError)
                }
            }
        }
    }
    
    func favorite(isFavorite: Bool, location: LocationItem) {
        if isFavorite {
            _ = Cache.shared.removeValue(forKey: location.locationId)
        } else {
            Cache.shared.add(value: try? PropertyListEncoder().encode(location), forKey: location.locationId)
        }
        setFavoriteId(locationId: location.locationId, addingToFavorite: !isFavorite)
    }
    
    func favoriteIdsList() -> Array<String>? {
        return Cache.shared.value(forKey: MapProvider.favoriteListKey) as? Array<String>
    }
    
    private func setFavoriteId(locationId: String, addingToFavorite: Bool) {
        var favoriteIds: Array<String> = []
        if let ids = favoriteIdsList() {
            favoriteIds = ids
        }
        if !addingToFavorite, favoriteIds.count > 0 {
            favoriteIds.removeAll { $0 == locationId }
        } else {
            favoriteIds.append(locationId)
        }
        
        Cache.shared.add(value: favoriteIds, forKey: MapProvider.favoriteListKey)
    }
}
