//
//  MapProviderProtocol.swift
//  MyLocations
//
//  Created by Renato Miranda de Assis on 01/09/19.
//  Copyright © 2019 Renato Miranda. All rights reserved.
//

import Alamofire
import Foundation

typealias SuggestionsCallback = ([Suggestions]?, AFError?) -> Void
typealias LocationCallback = (LocationItem?, AFError?) -> Void

protocol MapProviderProtocol {
    func suggestedItems(searchText: String, completion: @escaping SuggestionsCallback)
    func location(locationId: String, completion: @escaping LocationCallback)
    func favorite(isFavorite: Bool, location: LocationItem)
    func favoriteIdsList() -> Array<String>?
}
