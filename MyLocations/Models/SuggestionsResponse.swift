//
//  SuggestionsResponse.swift
//  MyLocations
//
//  Created by Renato Miranda de Assis on 31/08/19.
//  Copyright © 2019 Renato Miranda. All rights reserved.
//

import Foundation

struct SuggestionsResponse: Codable {
    let suggestions: [Suggestions]
}

struct Suggestions: Codable {
    let label: String
    let address: Address
    let locationId: String
}

struct Address: Codable {
    let label: String?
    let street: String?
    let postalCode: String?
    let distance: String?
}
