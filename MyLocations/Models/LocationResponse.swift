//
//  LocationResponse.swift
//  MyLocations
//
//  Created by Renato Miranda de Assis on 31/08/19.
//  Copyright © 2019 Renato Miranda. All rights reserved.
//

import Foundation

struct Coordinate: Codable {
    let latitude: Double
    let longitude: Double
}

struct LocationResponse: Codable {
    let response: LocationInfo
}

struct LocationInfo: Codable {
    let view: [ViewResults]
}

struct ViewResults: Codable {
    let result: [LocationResult]
}

struct LocationResult: Codable {
    let relevance: Int
    let location: LocationItem
}

struct LocationItem: Codable {
    let locationId: String
    let displayPosition: Coordinate
    let address: Address
}
