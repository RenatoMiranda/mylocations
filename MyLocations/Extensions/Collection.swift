//
//  Collection.swift
//  MyLocations
//
//  Created by Renato Miranda de Assis on 31/08/19.
//  Copyright © 2019 Renato Miranda. All rights reserved.
//

import Foundation

extension Collection {
    
    /// Returns the element at the specified index iff it is within bounds, otherwise nil.
    ///
    /// - Parameter index: indexNumber
    subscript(safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
