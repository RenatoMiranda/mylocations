//
//  UIStoryboard.swift
//  MyLocations
//
//  Created by Renato Miranda de Assis on 31/08/19.
//  Copyright © 2019 Renato Miranda. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    /// Creates a view controller from storyboard
    ///
    /// - Parameters:
    ///   - storyboard: storyboard name
    /// - Returns: view controller instance
    static func viewController<T: UIViewController>(from storyboard: UIStoryboard.Name) -> T where T: Identifiable {
        let storyboard = UIStoryboard(name: storyboard.rawValue, bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: T.self.storyboardIdentifier) as? T else {
            fatalError("Could not instantiate ViewController with identifier: \(T.storyboardIdentifier)")
        }
        
        return viewController
    }
    
    /// Creates a view controller from storyboard with storyboard id
    ///
    /// - Parameters:
    ///   - storyboard: storyboard name
    /// - Returns: view controller instance
    static func viewController<T: UIViewController>(from storyboard: UIStoryboard.Name, id: String) -> T {
        let storyboard = UIStoryboard(name: storyboard.rawValue, bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: id) as? T else {
            fatalError("Could not instantiate ViewController with identifier: \(id)")
        }
        
        return viewController
    }
    
    enum Name: String {
        case main = "Main"
    }
}
