//
//  MapViewController.swift
//  MyLocations
//
//  Created by Renato Miranda de Assis on 31/08/19.
//  Copyright © 2019 Renato Miranda. All rights reserved.
//

import UIKit
import NMAKit

struct Location {
    var locationId: String
    var label: String
}

class MapViewController: UIViewController {

    private var location: Location?
    private var provider: MapProviderProtocol = MapProvider()
    private var marker: NMAMapMarker?
    private var currentCoordinates: NMAGeoCoordinates = NMAGeoCoordinates(latitude: 0, longitude: 0)
    private var locationItem: LocationItem?
    private var containsInCache = false
    
    @IBOutlet private weak var mapView: NMAMapView!
    @IBOutlet private weak var streetlabel: UILabel!
    @IBOutlet private weak var postalCodeLabel: UILabel!
    @IBOutlet private weak var latitudeLabel: UILabel!
    @IBOutlet private weak var longitudeLabel: UILabel!
    @IBOutlet private weak var distanceLabel: UILabel!
    
    static func instantiate(location: Location, provider: MapProviderProtocol = MapProvider()) -> MapViewController {
        let viewController: MapViewController = UIStoryboard.viewController(from: .main, id: String(describing: MapViewController.self))
        viewController.location = location
        viewController.provider = provider
        return viewController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationItem.title = location?.label
        setupNavigationItems()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let locationId = location?.locationId else { return }
        provider.location(locationId: locationId) { [weak self] (item, error) in
            guard let item = item else { return }
            self?.locationItem = item
            if let street = item.address.street {
                self?.streetlabel.text = "Street: \(street)"
            }
            
            if let postalCode = item.address.postalCode {
                self?.postalCodeLabel.text = "Postal Code: \(postalCode)"
            }
            
            if let distance = item.address.distance {
                self?.distanceLabel.text = "Distance: \(distance)"
            }
            
            self?.latitudeLabel.text = "Latitude: \(String(item.displayPosition.latitude))"
            self?.longitudeLabel.text = "Longitude: \(String(item.displayPosition.longitude))"
            self?.currentCoordinates = NMAGeoCoordinates(latitude: item.displayPosition.latitude, longitude: item.displayPosition.longitude)
            self?.setupMap()
        }
    }
    
    private func setupNavigationItems() {
        let backButton = UIBarButtonItem(title: "Back", style: .done, target: self, action: #selector(back))
        navigationItem.leftBarButtonItem = backButton
        var imageName = "star_empty"
        containsInCache = false
        if let locationId = location?.locationId, Cache.shared.contains(locationId) {
            imageName = "star"
            containsInCache = true
        }
        let favoriteButton = UIBarButtonItem.menuButton(self, action: #selector(favoriteAlert), imageName: imageName)
        navigationItem.rightBarButtonItem = favoriteButton
    }
    
    private func addMapCircle() {
        let marker = NMAMapMarker(geoCoordinates: currentCoordinates, image: resizeImage(image: UIImage(named: "marker")!, newWidth: 20))
        mapView.add(marker)
    }
    
    @objc
    private func back() {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @objc
    private func favoriteAlert() {
        let alertController = UIAlertController(title: getTitleAlert(), message: getMessageAlert(), preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Yes", style: .default) { [weak self] (action:UIAlertAction) in
            print("You've pressed yes");
            guard let location = self?.locationItem else { return }
            self?.provider.favorite(isFavorite: self?.containsInCache ?? false, location: location)
            self?.setupNavigationItems()
        }
        
        let action2 = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction) in
            print("You've pressed no");
        }
        alertController.addAction(action1)
        alertController.addAction(action2)
        present(alertController, animated: true, completion: nil)
    }
    
    private func setupMap() {
        mapView.useHighResolutionMap = true
        mapView.zoomLevel = 13.2
        mapView.set(geoCenter: currentCoordinates,
                    animation: .linear)
        mapView.copyrightLogoPosition = NMALayoutPosition.bottomCenter
        addMapCircle()
    }
    
    private func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    private func getTitleAlert() -> String {
        var alertTitle = "Add to favorites"
        if containsInCache {
            alertTitle = "Remove from Favorites"
        }
        
        return alertTitle
    }
    
    private func getMessageAlert() -> String {
        var alertMessage = "would you like to add \(location?.label ?? "") to favorites?"
        if containsInCache {
            alertMessage = "would you like to remove \(location?.label ?? "") to favorites?"
        }
        
        return alertMessage
    }
}
