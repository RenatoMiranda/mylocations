//
//  FavoriteListViewController.swift
//  MyLocations
//
//  Created by Renato Miranda de Assis on 01/09/19.
//  Copyright © 2019 Renato Miranda. All rights reserved.
//

import UIKit
import NMAKit

class FavoriteListViewController: UIViewController {

    private var provider: MapProviderProtocol = MapProvider()
    private var favoriteItems: [LocationItem] = []
    
    @IBOutlet private weak var tableView: UITableView?
    @IBOutlet private weak var mapView: NMAMapView!
    
    static func instantiate(provider: MapProviderProtocol = MapProvider()) -> FavoriteListViewController {
        let viewController: FavoriteListViewController = UIStoryboard.viewController(from: .main, id: String(describing: FavoriteListViewController.self))
        viewController.provider = provider
        return viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let ids = provider.favoriteIdsList() {
            for id in ids {
                provider.location(locationId: id) { [weak self] (item, error) in
                    if let location = item {
                        self?.favoriteItems.append(location)
                    }
                    self?.setupMap()
                }
            }
        }
    }
    
    @IBAction func closeButtonTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    private func setupMap() {
        mapView.useHighResolutionMap = true
        mapView.zoomLevel = 13.2
        if let firstItem = favoriteItems.first {
            mapView.set(geoCenter: NMAGeoCoordinates(latitude: firstItem.displayPosition.latitude, longitude: firstItem.displayPosition.longitude),
                        animation: .linear)
        }
        mapView.copyrightLogoPosition = NMALayoutPosition.bottomCenter
    }
    
    private func addMapCircle(coordinates: Coordinate) {
        let marker = NMAMapMarker(geoCoordinates: NMAGeoCoordinates(latitude: coordinates.latitude, longitude: coordinates.longitude), image: resizeImage(image: UIImage(named: "marker")!, newWidth: 20))
        mapView.add(marker)
    }
    
    private func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

extension FavoriteListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoriteItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        if let location = favoriteItems[safe: indexPath.row] {
            cell.textLabel?.text = location.address.label
        }
        
        return cell
    }
}

extension FavoriteListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let item = favoriteItems[safe: indexPath.row] {
            mapView.set(geoCenter: NMAGeoCoordinates(latitude: item.displayPosition.latitude, longitude: item.displayPosition.longitude),
                        animation: .linear)
            addMapCircle(coordinates: item.displayPosition)
        }
    }
}
