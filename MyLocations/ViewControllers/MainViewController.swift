//
//  MainViewController.swift
//  MyLocations
//
//  Created by Renato Miranda de Assis on 31/08/19.
//  Copyright © 2019 Renato Miranda. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView?
    @IBOutlet private weak var goToFavoritesButton: UIButton?
    
    private var provider: MapProviderProtocol = MapProvider()
    private var items: [Suggestions] = []
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    static func instantiate(provider: MapProviderProtocol = MapProvider()) -> MainViewController {
        let viewController: MainViewController = UIStoryboard.viewController(from: .main, id: String(describing: MainViewController.self))
        viewController.provider = provider
        return viewController
    }
    
    @IBAction private func goToFavoritesTap(_ sender: Any) {
        let viewController = FavoriteListViewController.instantiate()
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: true, completion: nil)
    }
    
    private func searchItems(text: String) {
        provider.suggestedItems(searchText: text) { [weak self] (response, error) in
            guard let response = response else { return }
            self?.items = []
            self?.items.append(contentsOf: response)
            self?.tableView?.reloadData()
        }
    }
}

extension MainViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchItems(text: searchText)
    }
}

extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let item = items[safe: indexPath.row] {
            let viewController = MapViewController.instantiate(location: Location(locationId: item.locationId, label: item.label), provider: provider)
            navigationController?.pushViewController(viewController, animated: true)
        }
        self.tableView?.deselectRow(at: indexPath, animated: false)
    }
}

extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        if let item = items[safe: indexPath.row] {
            cell.textLabel?.text = item.label
        }
        
        return cell
    }
    
    
}

