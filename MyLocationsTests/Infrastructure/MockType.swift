//
//  MockType.swift
//  MyLocationsTests
//
//  Created by Renato Miranda de Assis on 01/09/19.
//  Copyright © 2019 Renato Miranda. All rights reserved.
//

import Foundation

enum MockType {
    case valid
    case invalid
}
