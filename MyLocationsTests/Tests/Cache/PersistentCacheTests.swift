//
//  PersistentCacheTests.swift
//  MyLocationsTests
//
//  Created by Renato Miranda de Assis on 01/09/19.
//  Copyright © 2019 Renato Miranda. All rights reserved.
//

import Foundation

import XCTest
@testable import MyLocations

struct UserCache: Codable {
    var identifier: Int
    var name: String
}


final class PersistentCacheTestes: XCTestCase {
    private let cacheKey = "b70060ee-9e6e-4f29-97a7-4bf80bf2601b"
    private let value = 1234.0
    
    private func fillCache(_ count: Int) {
        let keyPrefix = "countSessionTestKey"
        let value: Int = 123_456
        
        for index in 0 ..< count {
            let key = keyPrefix + String(index)
            Cache.shared.add(value: value,
                             forKey: key)
        }
    }
    
    override func setUp() {
        super.setUp()
        
        Cache.setup()
        Cache.shared.clear()
    }
    
    override func tearDown() {
        super.tearDown()
        
        Cache.shared.clear()
    }
    
    func testStoreValueTypeForKey() {
        Cache.shared.add(value: value, forKey: cacheKey)
        guard let recoveredValue = Cache.shared.value(forKey: cacheKey) as? Double else {
            XCTFail("Could not parse recoveredObject")
            return
        }
        XCTAssertEqual(recoveredValue, value)
    }
    
    func testStoreReferenceTypeForKey() {
        let user = UserCache(identifier: 123, name: "John")
        
        Cache.shared.add(value: try? PropertyListEncoder().encode(user),
                         forKey: cacheKey)
        guard let recoveredValue = Cache.shared.value(forKey: cacheKey) as? Data else {
            XCTFail("Could not parse recoveredObject")
            return
        }
        
        guard let recoveredUser = try? PropertyListDecoder().decode(UserCache.self, from: recoveredValue) else {
            XCTFail("Could not parse recoveredObject")
            return
        }
        
        XCTAssertEqual(recoveredUser.identifier, user.identifier)
        XCTAssertEqual(recoveredUser.name, user.name)
    }
    
    func testCountItems() {
        let initialCount = Cache.shared.count()
        fillCache(5)
        let count = Cache.shared.count()
        XCTAssertEqual(count, initialCount + 5)
    }
    
    func testClearCache() {
        let initialCount = Cache.shared.count()
        fillCache(5)
        Cache.shared.clear()
        let count = Cache.shared.count()
        XCTAssertEqual(count, initialCount)
    }
    
    func testRemoveValueForKey() {
        let initialCount = Cache.shared.count()
        Cache.shared.add(value: value, forKey: cacheKey)
        XCTAssertEqual(Cache.shared.count(), initialCount + 1)
        guard let (_, removed) = Cache.shared.removeValue(forKey: cacheKey) as? (Any?, Double) else {
            XCTFail("Could not parse recoveredObject")
            return
        }
        XCTAssertEqual(removed, value)
        XCTAssertEqual(Cache.shared.count(), initialCount)
    }
    
    func testShouldNotRemoveValueForInvalidKey() {
        let initialCount = Cache.shared.count()
        Cache.shared.add(value: value, forKey: cacheKey)
        XCTAssertEqual(Cache.shared.count(), initialCount + 1)
        let (_, removed) = Cache.shared.removeValue(forKey: "someDiferentKey")
        XCTAssertNil(removed)
        XCTAssertEqual(Cache.shared.count(), initialCount + 1)
    }
    
    func testContainsKey() {
        Cache.shared.add(value: value, forKey: cacheKey)
        XCTAssertTrue(Cache.shared.contains(cacheKey))
    }
}
