//
//  FavoriteListViewControllerTests.swift
//  MyLocationsTests
//
//  Created by Renato Miranda de Assis on 01/09/19.
//  Copyright © 2019 Renato Miranda. All rights reserved.
//

import XCTest
@testable import MyLocations

class FavoriteListViewControllerTests: BaseXCTest {
    
    private var viewController: FavoriteListViewController?
    
    override func tearDown() {
        super.tearDown()
        viewController?.dismiss(animated: false, completion: nil)
    }
    
    override func setUp() {
        super.setUp()
        let provider = MapProviderMock(mockType: .valid)
        provider.favorite(isFavorite: true, location: LocationItem(locationId: "1", displayPosition: Coordinate(latitude: -1.0, longitude: 1.0), address: Address(label: "Brasil, São Paulo", street: "Rua São Paulo", postalCode: "31230-123", distance: "100")))
        
        provider.favorite(isFavorite: true, location: LocationItem(locationId: "2", displayPosition: Coordinate(latitude: -1.0, longitude: 1.0), address: Address(label: "Brasil, Espírito Santo", street: "Rua Espírito Santo", postalCode: "31230-123", distance: "100")))
        
        provider.favorite(isFavorite: true, location: LocationItem(locationId: "3", displayPosition: Coordinate(latitude: -1.0, longitude: 1.0), address: Address(label: "Brasil, Belo Horizonte", street: "Rua Belo Horizonte", postalCode: "31230-123", distance: "100")))
        viewController = FavoriteListViewController.instantiate(provider: provider)
    }
    
    func testInitialization() {
        verifySnapshotView(delay: 0.2) {
            self.viewController?.view
        }
    }
}
