//
//  MyLocationsTests.swift
//  MyLocationsTests
//
//  Created by Renato Miranda de Assis on 31/08/19.
//  Copyright © 2019 Renato Miranda. All rights reserved.
//

import XCTest
@testable import MyLocations

class MainViewControllerTests: BaseXCTest {

    private var viewController: MainViewController?
    
    override func tearDown() {
        super.tearDown()
        viewController?.dismiss(animated: false, completion: nil)
    }
    
    override func setUp() {
        super.setUp()
        let provider = MapProviderMock(mockType: .valid)
        viewController = MainViewController.instantiate(provider: provider)
    }

    func testInitialization() {
        viewController?.searchBar(UISearchBar(), textDidChange: "São")
        verifySnapshotView(delay: 0.2) {
            self.viewController?.view
        }
    }
    
    func testSelectItem() {
        let provider = MapProviderMock(mockType: .valid)
        let controller = MainViewController.instantiate(provider: provider)
        let navigation = UINavigationController(rootViewController: controller)
        viewController = controller
        
        viewController?.searchBar(UISearchBar(), textDidChange: "São")
        viewController?.tableView(UITableView(), didSelectRowAt: IndexPath(row: 0, section: 0))
        verifySnapshotView(delay: 0.2) {
            navigation.view
        }
    }
    
    func testCellForRow() {
        viewController?.searchBar(UISearchBar(), textDidChange: "São")
        let cell = viewController?.tableView(UITableView(), cellForRowAt: IndexPath(row: 0, section: 0))
        cell?.textLabel?.text = "Brasil, São Paulo"
    }
}
