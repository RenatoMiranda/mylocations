//
//  MapViewControllerTests.swift
//  MyLocationsTests
//
//  Created by Renato Miranda de Assis on 01/09/19.
//  Copyright © 2019 Renato Miranda. All rights reserved.
//

import XCTest
@testable import MyLocations

class MapViewControllerTests: BaseXCTest {
    private var viewController: MapViewController?
    private var navigation: UINavigationController?
    
    override func tearDown() {
        super.tearDown()
        viewController?.dismiss(animated: false, completion: nil)
    }
    
    override func setUp() {
        super.setUp()
        
        let provider = MapProviderMock(mockType: .valid)
        let controller = MainViewController.instantiate(provider: provider)
        navigation = UINavigationController(rootViewController: controller)
        let expectation = XCTestExpectation(description: "")
        viewController = MapViewController.instantiate(location: Location(locationId: "1", label: "Brasil"), provider: provider)
        
        UIApplication.shared.keyWindow?.rootViewController?.present(navigation!, animated: false) {
            controller.navigationController?.pushViewController(self.viewController!, animated: false)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }
    
    func testInitialization() {
        verifySnapshotView(delay: 1, identifier: "navigationBar") {
            self.navigation?.navigationBar
        }
        verifySnapshotView(delay: 1, identifier: "view") {
            self.viewController?.view
        }
    }
}
