//
//  MapProviderMock.swift
//  MyLocationsTests
//
//  Created by Renato Miranda de Assis on 01/09/19.
//  Copyright © 2019 Renato Miranda. All rights reserved.
//

import Alamofire
import Foundation
@testable import MyLocations

class MapProviderMock: MapProviderProtocol {
    var mockType: MockType
    private var count = 0
    
    required init(mockType: MockType) {
        self.mockType = mockType
    }
    
    private var listItems: [String: LocationItem] = [:]
    private var listIds: [String] = []
    
    func suggestedItems(searchText: String, completion: @escaping SuggestionsCallback) {
        let suggestions = [Suggestions(label: "Brasil, São Paulo", address: Address(label: "Brasil, São Paulo", street: "Rua São Paulo", postalCode: "31230-123", distance: "100"), locationId: "1"), Suggestions(label: "Brasil, Belo Horizonte", address: Address(label: "Brasil, Belo Horizonte", street: "Rua Belo Horizonte", postalCode: "31230-123", distance: "100"), locationId: "2"), Suggestions(label: "Brasil, Rio de Janeiro", address: Address(label: "Brasil", street: "Rua Rio de Janeiro", postalCode: "31230-123", distance: "100"), locationId: "3"), Suggestions(label: "Brasil, Espírito Santo", address: Address(label: "Brasil, Espírito Santo", street: "Rua Espírito Santo", postalCode: "31230-123", distance: "100"), locationId: "4")]
        
        switch mockType {
        case .valid:
            let filteredItems = suggestions.filter { $0.label.contains(searchText) }
            completion(filteredItems, nil)
        default:
            completion(nil, AFError.explicitlyCancelled)
        }
    }
    
    func location(locationId: String, completion: @escaping LocationCallback) {
        switch mockType {
        case .valid:
            if listItems.count > 0 {
                let location = listItems[locationId]
                count += 1
                completion(location, nil)
            } else {
                let location = LocationItem(locationId: "1", displayPosition: Coordinate(latitude: -1.0, longitude: 1.0), address: Address(label: "Brasil, Espírito Santo", street: "Rua Espírito Santo", postalCode: "31230-123", distance: "100"))
                completion(location, nil)
            }
        default:
            completion(nil, AFError.explicitlyCancelled)
        }
    }
    
    func favorite(isFavorite: Bool, location: LocationItem) {
        if isFavorite {
            listItems[location.locationId] = location
            listIds.append(location.locationId)
        } else {
            listItems.removeValue(forKey: location.locationId)
            listIds.removeAll { $0 == location.locationId }
        }
    }
    
    func favoriteIdsList() -> Array<String>? {
        return listIds
    }
}
